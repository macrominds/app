<?php


namespace Macrominds\Testing;

use Macrominds\App\ContainerProvider;
use Macrominds\Services\Container;
use Pimple\Container as PimpleContainer;

trait ProvidesContainer
{
    /**
     * Gets called before each test
     * @before
     */
    protected function setupContainerProvider(): void
    {
        $this->resetContainerProvider();
        $this->initializeContainerProvider();
    }

    protected function resetContainerProvider(): void
    {
        ContainerProvider::forgetInstance();
    }

    abstract protected function initializeContainerProvider(): void;

    protected function getContainer(): Container
    {
        return ContainerProvider::getInstance();
    }

    protected function newContainer(): Container
    {
        return new Container(new PimpleContainer());
    }
}
