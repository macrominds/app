<?php


namespace Macrominds\Testing;

trait RefreshEnv
{
    /** @var array */
    private $envBackup = [];
    /** @var array */
    private $serverBackup;

    /**
     * @before
     */
    public function backupEnv()
    {
        $this->envBackup = array_merge($_ENV);
        $this->serverBackup = array_merge($_SERVER);
    }

    /**
     * @after
     */
    public function restoreEnv()
    {
        $this->loadEnvBackup();
    }

    private function loadEnvBackup()
    {
        $_ENV = $this->envBackup;
        $_SERVER = $this->serverBackup;
    }
}
