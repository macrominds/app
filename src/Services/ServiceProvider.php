<?php

namespace Macrominds\Services;

interface ServiceProvider
{
    public function register(Container $container);
}
