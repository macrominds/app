<?php


namespace Macrominds\Services;

use Macrominds\App\ProvidesHasContainerContract;

trait AllowsServiceRegistration
{
    use ProvidesHasContainerContract;

    public function registerServiceProviders(array $serviceProviders)
    {
        foreach ($serviceProviders as $serviceProvider) {
            $this->registerServiceProvider($serviceProvider);
        }
    }

    public function registerServiceProvider(ServiceProvider $serviceProvider)
    {
        $this->getContainer()
            ->registerServiceProvider($serviceProvider);
    }
}
