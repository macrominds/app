<?php

namespace Macrominds\Services;

class Container extends PimpleContainerDecorator
{
    public function factoryObject(string $keyOrClass, callable $factory): Container
    {
        $this[$keyOrClass] = $this->factory($factory);

        return $this;
    }

    public function singleton(string $keyOrClass, callable $factory): Container
    {
        $this[$keyOrClass] = $factory;

        return $this;
    }

    public function resolve(string $keyOrClassName)
    {
        return $this[$keyOrClassName];
    }

    public function registerServiceProvider(ServiceProvider $provider, array $values = [])
    {
        parent::register(new PimpleServiceProviderInterfaceAdapter($provider), $values);

        return $this;
    }
}
