<?php


namespace Macrominds\Services;

use RuntimeException;
use Throwable;

class MissingServiceException extends RuntimeException
{
    public function __construct(string $missingServiceName, $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            $this->createExceptionMessageFor($missingServiceName),
            $code,
            $previous
        );
    }

    private function createExceptionMessageFor(string $missingServiceName)
    {
        return <<<LOG
Missing service binding '$missingServiceName'.
Please bind to container or specify in ServiceContainer.
LOG;
    }
}
