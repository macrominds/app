<?php

namespace Macrominds\Services;

use Macrominds\Services\ServiceProvider as MacromindsServiceProvider;
use Pimple\Container as PimpleContainer;
use Pimple\ServiceProviderInterface as PimpleServiceProviderInterface;

class PimpleServiceProviderInterfaceAdapter implements PimpleServiceProviderInterface
{
    /**
     * @var MacromindsServiceProvider
     */
    private $provider;

    public function __construct(MacromindsServiceProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * {@inheritdoc}
     */
    public function register(PimpleContainer $pimple)
    {
        $macromindsContainer = $pimple;
        if (! ($pimple instanceof Container)) {
            $macromindsContainer = new Container($pimple);
        }
        $this->provider->register($macromindsContainer);
    }
}
