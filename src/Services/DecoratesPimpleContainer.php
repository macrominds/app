<?php

namespace Macrominds\Services;

use Pimple\Container as PimpleContainer;
use Pimple\Exception\UnknownIdentifierException;
use Pimple\ServiceProviderInterface;

/**
 * Trait DecoratesPimpleContainer.
 *
 * You need to call $this->decorate($pimpleContainer);
 * in the constructor of the using class!
 */
trait DecoratesPimpleContainer
{
    /**
     * @var PimpleContainer
     */
    private $decorated;

    private function decorate(PimpleContainer $decorated)
    {
        $this->decorated = $decorated;
    }

    public function offsetGet($id): mixed
    {
        try {
            return $this->decorated->offsetGet($id);
        } catch (UnknownIdentifierException $e) {
            throw new MissingServiceException($id, 0, $e);
        }
    }

    public function offsetExists($id): bool
    {
        return $this->decorated->offsetExists($id);
    }

    public function offsetSet($id, $value): void
    {
        $this->decorated->offsetSet($id, $value);
    }

    public function offsetUnset($id): void
    {
        $this->decorated->offsetUnset($id);
    }

    public function factory($callable)
    {
        return $this->decorated->factory($callable);
    }

    public function protect($callable)
    {
        return $this->decorated->protect($callable);
    }

    public function raw($id)
    {
        return $this->decorated->raw($id);
    }

    public function extend($id, $callable)
    {
        return $this->decorated->extend($id, $callable);
    }

    public function keys()
    {
        return $this->decorated->keys();
    }

    public function register(ServiceProviderInterface $provider, array $values = [])
    {
        $this->decorated->register($provider, $values);

        return $this;
    }
}
