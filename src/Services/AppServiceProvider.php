<?php


namespace Macrominds\Services;

abstract class AppServiceProvider implements ServiceProvider
{
    abstract public function getSingletonServices(Container $c): array;
    abstract public function getFactoryObjectServices(Container $c): array;

    /**
     * @var array
     */
    private $configOverride;

    public function __construct(array $configOverride = [])
    {
        $this->configOverride = $configOverride;
    }

    public function register(Container $container)
    {
        foreach ($this->getParameters() as $parameter => $value) {
            $container[$parameter] = is_callable($value) ? $container->protect($value) : $value;
        }
        foreach ($this->getServices($container) as $service => $implementation) {
            $container[$service] = $implementation;
        }
    }

    public function getParameters(): array
    {
        return [
            'config' => array_merge([], $this->configOverride),
        ];
    }

    public function getServices(Container $c): array
    {
        return array_merge($this->getSingletonServices($c), $this->getFactoryObjectServices($c));
    }
}
