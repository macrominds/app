<?php

namespace Macrominds\Services;

use Pimple\Container as PimpleContainer;

class PimpleContainerDecorator extends PimpleContainer
{
    use DecoratesPimpleContainer;

    public function __construct(PimpleContainer $decorated)
    {
        $this->decorate($decorated);
        parent::__construct();
    }
}
