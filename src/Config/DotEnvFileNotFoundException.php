<?php

namespace Macrominds\Config;

use RuntimeException;

class DotEnvFileNotFoundException extends RuntimeException
{
}
