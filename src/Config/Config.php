<?php

namespace Macrominds\Config;

class Config
{
    private $config;

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    public function get(string $key, $fallback = null)
    {
        return $this->getWithOptionalDotSyntax($this->config, $key, $fallback);
    }

    private function getWithOptionalDotSyntax(array $array, string $key, $fallback = null)
    {
        // inlined Macrominds\Helpers\Arr::get to prevent extra dependency
        if (! isset($array[$key]) && false !== strpos($key, '.')) {
            $keys = explode('.', $key, 2);
            $r = $array[$keys[0]] ?? null;
            if (is_array($r)) {
                return $this->getWithOptionalDotSyntax($r, $keys[1], $fallback);
            }
        }

        return $array[$key] ?? $fallback;
    }

    public function set(string $key, $value)
    {
        $this->config[$key] = $value;
    }

    public function putAll(array $config)
    {
        $this->config = array_merge($this->config, $config);
    }

    public function reset(array $config)
    {
        $this->config = $config;
    }
}
