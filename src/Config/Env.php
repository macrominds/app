<?php

namespace Macrominds\Config;

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

class Env
{
    /**
     * Env constructor.
     *
     * @param string      $basePath         for .env file (usually project root)
     * @param string|null $relativeFilename e.g. '.env', which is the default.
     */
    public function __construct(string $basePath, string $relativeFilename = '.env')
    {
        $this->makeDotEnvAvailableInEnv($basePath, $relativeFilename);
    }

    public function get(string $key, $fallback = false)
    {
        return $_ENV[$key] ?? $_SERVER[$key] ?? $fallback;
    }

    /**
     * @throws DotEnvFileNotFoundException
     */
    protected function makeDotEnvAvailableInEnv(string $basePath, string $relativeFilename)
    {
        try {
            // Note: I have chosen to use immutable instead of mutable, because it is necessary for
            // the testing environment to respect settings defined via phpunit.xml `<php><env>`,
            // such as the DB. Immutable means that these settings will not be overridden
            // once .env is loaded. Mutable would have meant that the settings would
            // be overridden, possibly leading us to using a production db in
            // the testing environment. Which is the last thing you want.
            Dotenv::createImmutable([$basePath], $relativeFilename)
                ->load();
        } catch (InvalidPathException $e) {
            if ($this->startsWith($relativeFilename, '/')) {
                throw new DotEnvFileNotFoundException("You specified the absolute dotEnv file path {$relativeFilename}. Please provide a dotEnv file name _relative_ to {$basePath}.", $e->getCode(), $e);
            }
            throw new DotEnvFileNotFoundException("Could not load dotEnv file '{$relativeFilename}' in '{$basePath}'.", $e->getCode(), $e);
        }
    }

    private function startsWith(string $haystack, string $needle)
    {
        return 0 === strpos($haystack, $needle);
    }
}
