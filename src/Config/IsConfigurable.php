<?php


namespace Macrominds\Config;

use Macrominds\App\ProvidesHasContainerContract;

trait IsConfigurable
{
    use ProvidesHasContainerContract;

    public function configure(array $config): self
    {
        $this->getConfig()->putAll($config);

        return $this;
    }

    private function getConfig(): Config
    {
        return $this->getContainer()->resolve(Config::class);
    }

    private function registerDefaultConfig(): void
    {
        $this->getContainer()->singleton(
            Config::class,
            function () {
                return new Config();
            }
        );
    }
}
