<?php


namespace Macrominds\Config;

use Macrominds\App\ProvidesHasContainerContract;

trait UsesDotEnv
{
    use ProvidesHasContainerContract;

    private function registerDotEnv(string $projectPath, string $relativeFilename = '.env'): void
    {
        $this->getContainer()->singleton(
            Env::class,
            function () use ($projectPath, $relativeFilename): Env {
                return new Env($projectPath, $relativeFilename);
            }
        );
    }
    private function getEnv(): Env
    {
        return $this->getContainer()->resolve(Env::class);
    }
}
