<?php

namespace Macrominds\App;

use Macrominds\Services\Container;

trait HasContainer
{
    use ProvidesHasContainerContract;

    /**
     * @var Container
     */
    protected $container;

    private function provideContainer(Container $container = null): Container
    {
        return $container ?? ContainerProvider::getInstance();
    }

    public function getContainer(): Container
    {
        return $this->container;
    }
}
