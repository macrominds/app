<?php


namespace Macrominds\App;

use Macrominds\Services\Container;

trait ProvidesHasContainerContract
{
    abstract public function getContainer(): Container;
}
