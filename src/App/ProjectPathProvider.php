<?php

namespace Macrominds\App;

interface ProjectPathProvider
{
    public function getProjectPath(string $subPathWithLeadingSlash = null): string;
}
