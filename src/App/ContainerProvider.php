<?php

namespace Macrominds\App;

use Macrominds\Services\Container;
use Pimple\Container as PimpleContainer;

class ContainerProvider
{
    /**
     * @var Container
     */
    private static $instance;

    public static function getInstance(): Container
    {
        if (null === static::$instance) {
            static::$instance = new Container(new PimpleContainer());
        }

        return static::$instance;
    }

    public static function forgetInstance()
    {
        static::$instance = null;
    }
}
