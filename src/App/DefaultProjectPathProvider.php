<?php

namespace Macrominds\App;

use InvalidArgumentException;

class DefaultProjectPathProvider implements ProjectPathProvider
{
    /**
     * @var string
     */
    private $projectPath;

    public function __construct(string $projectPath)
    {
        $this->projectPath = $projectPath;
    }

    public function getProjectPath(string $subPathWithLeadingSlash = null): string
    {
        if (null === $subPathWithLeadingSlash) {
            return $this->projectPath;
        }

        return $this->relativeProjectPath($subPathWithLeadingSlash);
    }

    /**
     * @param string $subPathWithLeadingSlash the path, relative to the project directory. Needs to start with '/'.
     *
     *@throws InvalidArgumentException if the parameter doesn't start with a slash
     *
     * @return string the absolute path
     */
    public function relativeProjectPath(string $subPathWithLeadingSlash): string
    {
        if (! $this->startsWith($subPathWithLeadingSlash, '/')) {
            throw new InvalidArgumentException("The relative path '{$subPathWithLeadingSlash}' needs to start with '/'.");
        }

        return $this->projectPath.$subPathWithLeadingSlash;
    }

    private function startsWith(string $haystack, string $needle)
    {
        return 0 === strpos($haystack, $needle);
    }
}
