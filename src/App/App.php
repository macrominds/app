<?php

namespace Macrominds\App;

abstract class App
{
    use HasContainer;

    public function __construct(string $projectPath)
    {
        $this->container = $this->provideContainer();
        $this->registerServices($projectPath);
    }

    abstract protected function registerServices(string $projectPath);
}
