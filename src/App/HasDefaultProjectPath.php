<?php


namespace Macrominds\App;

/**
 * Provides a ProjectPath. It is mandatory to register
 * it with `$this->registerProjectPathProvider($pathWithLeadingSlash);`
 * usually in the constructor of the App using this.
 */
trait HasDefaultProjectPath
{
    use ProvidesHasContainerContract;

    private function registerProjectPathProvider(string $projectPath): void
    {
        $this->getContainer()->singleton(
            ProjectPathProvider::class,
            function () use ($projectPath): ProjectPathProvider {
                return new DefaultProjectPathProvider($projectPath);
            }
        );
    }

    private function getProjectPath(string $subPathWithLeadingSlash = null): string
    {
        /** @var $projectPathProvider ProjectPathProvider */
        $projectPathProvider = $this->getContainer()
            ->resolve(ProjectPathProvider::class);
        return $projectPathProvider->getProjectPath($subPathWithLeadingSlash);
    }
}
