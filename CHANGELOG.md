# Change Log

All noteable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [2.0.0] - 2023-09-17

- build(php)!: drop support for php7 and add support for php8.1 and php8.2

## [1.0.0] - 2021-04-11

- refactor(ci): reduce code duplication
- chore(testing): migrate phpunit.xml.dist to replace deprecations

Actually, there's no breaking change that justifies the major version upgrade.
I just felt, it was time for v1.0.0.

## [0.3.0] – 2021-04-09

- Enable [renovate bot](https://www.whitesourcesoftware.com/free-developer-tools/renovate) for automatic dependency update merge requests.
- Allow reusing of gitlab-ci scripts

## [0.2.1] – 2021-02-08

Make php 8.0 compatible, complement README and fix CI.

## [0.2.0] – 2020-06-12

- Prevent DotEnv from overriding existing env values

The main reason to change the behavior is this:
We want to be able to define env variables in the phpunit.xml config. But DotEnv would override these settings when the `.env` file gets loaded. Therefore, we changed DotEnv from mutable to immutable behavior. Existing env variables cannot be overridden anymore now.

This change forced us to back up and restore env values in between related tests. You can use this in dependent libs as well: simply add the trait `RefreshEnv` to your TestCase.

The new v5 of phpdotenv excludes getenv / putenv per default and discourages the usage, because these functions are not thread safe. Using v5 of phpdotenv made it easier to fix the immutable issue.

## [0.1.0] – 2020-05-02

- Extracted the app common lib from website-lib
- Dropped php 7.2 support to be able to use the recent phpunit 
