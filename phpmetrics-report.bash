#!/usr/bin/env bash

# DIR is only needed for the output.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

JUNIT=""
if [[ -f report/junit.xml ]]; then
  JUNIT="--junit=report/junit.xml"
fi
phpmetrics --git --report-html=report/phpmetrics/html --report-csv=report/phpmetrics/metrics.csv ${JUNIT} ./
echo ""
echo "file://${DIR}/report/phpmetrics/html/index.html"
echo ""

