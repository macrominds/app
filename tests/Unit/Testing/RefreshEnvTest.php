<?php


namespace Tests\Unit\Testing;

use Macrominds\Testing\RefreshEnv;
use Tests\TestCase;

class RefreshEnvTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideEnvSuperGlobalsAndCorrespondingFetchers
     */
    public function it_backs_up_existing_entries(array $arr, callable $fetcher)
    {
        $this->assertBackupPresent(
            $arr,
            array_rand($arr),
            $fetcher
        );
    }

    public function provideEnvSuperGlobalsAndCorrespondingFetchers()
    {
        return [
            '$_ENV' => [ &$_ENV, function (EnvRefresher $refresher) {
                return $refresher->getEnvBackup();
            } ],
            '$_SERVER' => [ &$_SERVER, function (EnvRefresher $refresher) {
                return $refresher->getServerBackup();
            } ],
        ];
    }

    private function assertBackupPresent(array $array, $key, callable $getBackup)
    {
        $expectedEnv = $array[$key];
        $refresher = new EnvRefresher();
        $refresher->backupEnv();
        $this->assertSame(
            $expectedEnv,
            $getBackup($refresher)[$key]
        );
    }

    /**
     * @test
     * @dataProvider provideEnvSuperGlobalsAndCorrespondingFetchers
     */
    public function it_restores_backed_up_entries(array &$arr)
    {
        $KEY = 'RefreshEnvTest';
        $refresher = new EnvRefresher();
        $arr[$KEY] = true;
        $refresher->backupEnv();
        $arr[$KEY] = false;
        $this->assertFalse($arr[$KEY]);
        $refresher->restoreEnv();
        $this->assertTrue($arr[$KEY]);
    }
}

class EnvRefresher
{
    use RefreshEnv;

    /**
     * @return array
     */
    public function getEnvBackup(): array
    {
        return $this->envBackup;
    }

    /**
     * @return array
     */
    public function getServerBackup(): array
    {
        return $this->serverBackup;
    }
}
