<?php


namespace Tests\Unit\Config;

use Macrominds\App\HasContainer;
use Macrominds\Config\UsesDotEnv;
use Macrominds\Testing\RefreshEnv;
use Tests\TestCase;

class UsesDotEnvTest extends TestCase
{
    use RefreshEnv;
    
    /** @test */
    public function it_registers_dot_env()
    {
        $app = new UsingDotEnv(
            $this->getFixturesPath('/config/dotenv'),
            'app-env-local'
        );
        $this->assertSame(
            'local',
            $app->exposeEnv()->get('APP_ENV')
        );
    }
}

class UsingDotEnv
{
    use HasContainer;
    use UsesDotEnv;

    public function __construct(string $projectPath, string $relativeFilename)
    {
        $this->container = $this->provideContainer();
        $this->registerDotEnv($projectPath, $relativeFilename);
    }

    public function exposeEnv()
    {
        return $this->getEnv();
    }
}
