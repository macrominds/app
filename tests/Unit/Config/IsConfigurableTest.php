<?php

namespace Tests\Unit\Config;

use Macrominds\App\HasContainer;
use Macrominds\Config\Config;
use Macrominds\Config\IsConfigurable;
use Tests\TestCase;

class IsConfigurableTest extends TestCase
{
    /** @test */
    public function it_retrieves_a_configuration_instance()
    {
        $this->assertInstanceOf(
            Config::class,
            (new Configurable())->exposeConfig()
        );
    }

    /** @test */
    public function it_allows_configurations()
    {
        $configurable = (new Configurable())->configure([
            'key' => 'value'
        ]);
        $this->assertSame(
            'value',
            $configurable->exposeConfig()->get('key')
        );
    }
}

class Configurable
{
    use HasContainer;
    use IsConfigurable;

    public function __construct()
    {
        $this->container = $this->provideContainer();
        $this->registerDefaultConfig();
    }

    public function exposeConfig(): Config
    {
        return $this->getConfig();
    }
}
