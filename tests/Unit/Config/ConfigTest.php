<?php

namespace Tests\Unit\Config;

use Macrominds\Config\Config;
use Tests\TestCase;

/**
 * @group wiring
 * @group config
 */
class ConfigTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidSettings
     */
    public function it_sets_and_gets_config_values($key, $value, $fallback, $expected)
    {
        $config = new Config();
        $config->set($key, $value);
        $this->assertEquals($expected, $config->get($key, $fallback));
    }

    public function provideValidSettings()
    {
        return [
            [
                'key',
                'value',
                null,
                'value',
            ],
            [
                'key',
                null,
                null,
                null,
            ],
            [
                'key',
                null,
                'fallback',
                'fallback',
            ],
            [
                'path.to.key',
                ['array-value'],
                null,
                ['array-value'],
            ],
        ];
    }

    /**
     * @test
     */
    public function we_can_mass_assign_an_array_in_the_constructor()
    {
        $c = [
            'path.to.key' => 'value',
            'override' => 'initial',
        ];
        $config = new Config($c);
        foreach ($c as $key => $value) {
            $this->assertEquals($value, $config->get($key));
        }
        $config->set('override', 'overridden');
        $this->assertEquals('overridden', $config->get('override'));
    }

    /**
     * @test
     */
    public function we_can_mass_assign_an_array()
    {
        $c = [
            'path.to.key' => 'value',
            'override' => 'overridden',
        ];
        $config = new Config();
        $config->set('override', 'initial');
        $this->assertEquals('initial', $config->get('override'));
        $config->putAll($c);
        foreach ($c as $key => $value) {
            $this->assertEquals($value, $config->get($key));
        }
        $config->reset(['path.to.key' => 'the-only-value']);
        $this->assertNull($config->get('override'));
        $this->assertEquals('the-only-value', $config->get('path.to.key'));
    }

    /**
     * @test
     */
    public function it_can_access_the_configs_using_dot_syntax()
    {
        $config = new Config([
            'path' => [
                'to' => [
                    'key' => 'dot',
                ],
            ],
        ]);
        $this->assertEquals('dot', $config->get('path.to.key'));
    }
}
