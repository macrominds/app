<?php

namespace Tests\Unit\Config;

use Macrominds\Config\DotEnvFileNotFoundException;
use Macrominds\Config\Env;
use Macrominds\Testing\RefreshEnv;
use Tests\TestCase;

/**
 * @group app
 */
class EnvTest extends TestCase
{
    use RefreshEnv;

    /**
     * @test
     * @group env
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_handles_non_existing_dot_env_files()
    {
        $this->expectException(DotEnvFileNotFoundException::class);
        $this->expectExceptionMessageMatches('/Could not load dotEnv file/');
        new Env(
            $this->getFixturesPath('/non-existing-dot-env-file')
        );
    }

    /**
     * @test
     * @group env
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_validates_that_the_env_filename_is_relative()
    {
        $this->expectException(DotEnvFileNotFoundException::class);
        $this->expectExceptionMessageMatches('/.+Please provide a dotEnv file name _relative_.+/');
        $dotEnvPath = '/dotenv/app-env-local';
        $absolutePath = $this->getConfigPath($dotEnvPath);
        new Env(
            $this->getFixturesPath(),
            $absolutePath
        );
    }

    /**
     * @test
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_forces_the_user_to_provide_a_dot_env_file()
    {
        $this->expectException(DotEnvFileNotFoundException::class);
        $this->expectExceptionMessageMatches('/Could not load dotEnv file/');
        $folderWithoutDotEnvFile = '/tests/fixtures';
        new Env(
            $this->getProjectRoot($folderWithoutDotEnvFile)
        );
    }

    /**
     * @test
     * @group env
     * @group config
     * @dataProvider provideDotEnvFixtures
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_reads_the_given_dot_env_file($dotEnvPath, $key, $expected)
    {
        $env = new Env(
            $this->getProjectRoot('/tests/fixtures'),
            $dotEnvPath
        );
        $this->assertEquals($expected, $env->get($key));
    }

    public function provideDotEnvFixtures(): array
    {
        return [
            'APP_ENV=local' => [
                'config/dotenv/app-env-local',
                'APP_ENV',
                'local',
            ],
            'APP_ENV=production' => [
                'config/dotenv/app-env-production',
                'APP_ENV',
                'production',
            ],
        ];
    }

    /**
     * @test
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_returns_false_if_env_var_is_not_present()
    {
        $env = new Env(
            $this->getProjectRoot('/tests/fixtures'),
            'config/dotenv/app-env-local'
        );
        $this->assertFalse($env->get('NOT_PRESENT'));
    }

    /**
     * @test
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_returns_the_fallback_value_if_env_var_is_not_present()
    {
        $env = new Env(
            $this->getProjectRoot('/tests/fixtures'),
            'config/dotenv/app-env-local'
        );
        $this->assertEquals('fallback', $env->get('NOT_PRESENT', 'fallback'));
    }
}
