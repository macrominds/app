<?php

namespace Tests\Unit\Services;

use Closure;
use Macrominds\Services\Container as MacromindsContainer;
use Macrominds\Services\DecoratesPimpleContainer;
use Pimple\Container as PimpleContainer;
use Pimple\ServiceProviderInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Tests\TestCase;

class ContainerDecoratorTest extends TestCase
{
    /**
     * @var MacromindsContainer
     */
    private $decorator;
    /**
     * @var PimpleContainer
     */
    private $decorated;

    /**
     * @before
     */
    public function setupDecorator()
    {
        $this->decorated = new PimpleContainer();
        $this->decorator = new MacromindsContainer($this->decorated);
    }

    /**
     * @test
     */
    public function it_can_be_constructed_with_a_pimple_container()
    {
        $this->assertInstanceOf(PimpleContainer::class, $this->decorator);
    }

    /**
     * @test
     */
    public function it_uses_the_factory_method_of_decorated()
    {
        $this->decorator['my-object'] = $this->decorator->factory(function (): Instance {
            return new Instance();
        });
        $first = $this->decorated['my-object'];
        $second = $this->decorated['my-object'];

        $this->assertResolved([$first, $second]);
        $this->assertNotSame($first, $second);
        $this->assertExpectedInstance([$first, $second]);
    }

    private function assertResolved(array $instances)
    {
        foreach ($instances as $instance) {
            $this->assertNotNull($instance);
        }
    }

    private function assertExpectedInstance(array $instances)
    {
        foreach ($instances as $instance) {
            $this->assertInstanceOf(Instance::class, $instance);
        }
    }

    /**
     * @test
     */
    public function it_uses_the_protect_method_of_decorated()
    {
        $this->assertSameCallableWithExpectedReturnValue(function ($key) {
            return $this->decorated[$key];
        });
    }

    private function assertSameCallableWithExpectedReturnValue(Closure $functionRetriever)
    {
        $expectedValue = 'new-value';
        $callable = function () use ($expectedValue) {
            return $expectedValue;
        };
        $this->decorator['new-key'] = $this->decorator->protect($callable);

        $function = $functionRetriever('new-key');

        $this->assertIsCallable($function);
        $this->assertSame($callable, $function);
        $this->assertEquals($expectedValue, $function());
    }

    /**
     * @test
     */
    public function it_uses_the_raw_method_of_decorated()
    {
        $this->assertSameCallableWithExpectedReturnValue(function ($key) {
            return $this->decorator->raw($key);
        });
    }

    /**
     * @test
     */
    public function it_uses_the_extend_method_of_decorated()
    {
        $this->decorator['my-object'] = $this->decorator->factory(function (): Instance {
            return new Instance();
        });

        $this->decorator->extend('my-object', function (Instance $instance) {
            $instance->value = 'new-value';

            return $instance;
        });

        $instance = $this->decorated['my-object'];
        $this->assertEquals('new-value', $instance->value);
    }

    /**
     * @test
     */
    public function it_uses_the_keys_method_of_decorated()
    {
        $this->decorated['key'] = 'value';
        $this->assertEquals(['key'], $this->decorator->keys());
    }

    /**
     * @test
     */
    public function it_uses_the_register_method_of_decorated()
    {
        $expectedValue = 'optional-value';
        $key = 'optional-key';
        $this->decorator->register(new PimpleServiceProvider(), [$key => $expectedValue]);
        // The registration works without overriding, because, effectively the service provider calls
        // a $container->offsetSet(…) / $container[...] = … and this array access is already
        // delegated to the decorated object. This is true for the optional values as
        // well. So we're not testing much here. But I think, for the sake of
        // completeness, we should test this method as well.
        $this->assertEquals(PimpleServiceProvider::VALUE, $this->decorated[PimpleServiceProvider::KEY]);
        $this->assertEquals($expectedValue, $this->decorated[$key]);
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function all_public_and_protected_methods_are_overridden()
    {
        // This test is important for future versions
        $pimpleContainerMethods = $this->getAllDirectPublicAndProtectedMethodsWithoutConstructor(PimpleContainer::class);
        $macromindsContainerMethods = $this->getAllDirectPublicAndProtectedMethodsWithoutConstructor(DecoratesPimpleContainer::class);
        $this->assertEquals(
            $pimpleContainerMethods,
            $macromindsContainerMethods
        );
    }

    /**
     * @throws ReflectionException
     */
    private function getAllDirectPublicAndProtectedMethodsWithoutConstructor(string $class)
    {
        $classInfo = new ReflectionClass($class);
        $result =
            array_map(
                function (ReflectionMethod $method) {
                    return $method->getName();
                },
                array_filter(
                    $classInfo->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED),
                    function (ReflectionMethod $method) use ($classInfo) {
                        return
                            $method->class === $classInfo->name &&
                            ! $method->isConstructor();
                    }
                )
            );
        sort($result);

        return $result;
    }
}

class Instance
{
    public $value = 'value';
}

class PimpleServiceProvider implements ServiceProviderInterface
{
    const KEY = 'my-pimple-key';
    const VALUE = 'my-pimple-value';

    /**
     * {@inheritdoc}
     */
    public function register(PimpleContainer $pimple)
    {
        $pimple[self::KEY] = self::VALUE;
    }
}
