<?php

namespace Tests\Unit\Services;

use Macrominds\BasicServiceProvider;
use Macrominds\Services\AppServiceProvider;
use Macrominds\Services\Container;
use Macrominds\Services\MissingServiceException;
use Tests\TestCase;

class ContainerUndefinedServiceTest extends TestCase
{
    /**
     * @test
     */
    public function it_throws_an_appropriate_exception_if_an_undefined_service_is_requested_using_array_access()
    {
        $container = $this->getContainer()->registerServiceProvider(new MockServiceProvider());
        $this->expectException(MissingServiceException::class);
        $container['undefined-service'];
    }

    /**
     * @test
     */
    public function it_throws_an_appropriate_exception_if_an_undefined_service_is_requested_using_the_resolve_method()
    {
        $container = $this->getContainer()->registerServiceProvider(new MockServiceProvider());
        $this->expectException(MissingServiceException::class);
        $container->resolve('undefined-service');
    }
}

class MockServiceProvider extends AppServiceProvider
{
    public function getSingletonServices(Container $c): array
    {
        return ['defined-singleton' => function () {
            return 'a-result';
        }];
    }

    public function getFactoryObjectServices(Container $c): array
    {
        return [
            'defined-factory-object' => $c->factory(function () {
                return 'a-factory-result';
            }),
        ];
    }
}
