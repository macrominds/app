<?php

namespace Tests\Unit\Services;

use Macrominds\Services\Container;
use Macrominds\Services\PimpleServiceProviderInterfaceAdapter;
use Macrominds\Services\ServiceProvider as MacromindsServiceProvider;
use Pimple\Container as PimpleContainer;
use Tests\TestCase;

class PimpleServiceProviderInterfaceAdapterTest extends TestCase
{
    /**
     * @before
     */
    public function resetMethodCallCount()
    {
        WatchingMacromindsServiceProvider::$numberOfCallsToRegister = 0;
    }

    /**
     * @test
     */
    public function it_adapts_when_used_with_a_MacromindsContainer()
    {
        $this->setupWithContainer(new Container(new PimpleContainer()));
        $this->assertRegisterMethodHasBeenCalled();
    }

    /**
     * @test
     */
    public function it_adapts_when_used_with_a_PimpleContainer()
    {
        $this->setupWithContainer(new PimpleContainer());
        $this->assertRegisterMethodHasBeenCalled();
    }

    private function assertRegisterMethodHasBeenCalled(int $expectedCalls = 1)
    {
        $actualCalls = WatchingMacromindsServiceProvider::$numberOfCallsToRegister;
        $this->assertEquals(
            $expectedCalls,
            $actualCalls,
            "Expected register method to be called {$expectedCalls} times. But it was called ${actualCalls} times."
        );
    }

    private function setupWithContainer(PimpleContainer $container)
    {
        $adapter = new PimpleServiceProviderInterfaceAdapter(new WatchingMacromindsServiceProvider());
        $adapter->register($container);

        return $adapter;
    }
}

class WatchingMacromindsServiceProvider implements MacromindsServiceProvider
{
    public static $numberOfCallsToRegister = 0;

    public function register(Container $container)
    {
        ++self::$numberOfCallsToRegister;
    }
}
