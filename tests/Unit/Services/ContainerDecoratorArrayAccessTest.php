<?php

namespace Tests\Unit\Services;

use Macrominds\Services\Container;
use Macrominds\Services\DecoratesPimpleContainer;
use Macrominds\Services\MissingServiceException;
use Pimple\Container as PimpleContainer;
use Pimple\Exception\UnknownIdentifierException;
use Tests\TestCase;

class ContainerDecoratorArrayAccessTest extends TestCase
{
    const CUSTOM_KEY = 'custom-key';
    const CUSTOM_VALUE = 'custom-value';
    const NON_EXISTENT_KEY = 'non-existent-key';
    /**
     * @var DecoratesPimpleContainer
     */
    private $decorator;

    /**
     * @before
     */
    public function setupDecorator()
    {
        $this->decorator = new Container(new PimpleContainer([
            self::CUSTOM_KEY => self::CUSTOM_VALUE,
        ]));
    }

    /**
     * @test
     */
    public function it_provides_offset_get()
    {
        $this->assertEquals(self::CUSTOM_VALUE, $this->decorator[self::CUSTOM_KEY]);

        $this->assertEquals(self::CUSTOM_VALUE, $this->decorator->offsetGet(self::CUSTOM_KEY));
    }

    /**
     * @test
     */
    public function it_throws_the_expected_exception_when_trying_to_get_a_non_existing_key_using_array_access()
    {
        $this->expectException(MissingServiceException::class);
        $this->decorator[self::NON_EXISTENT_KEY];
    }

    /**
     * @test
     */
    public function it_throws_the_expected_exception_when_trying_to_get_a_non_existing_key()
    {
        $this->expectException(MissingServiceException::class);
        $this->decorator->offsetGet(self::NON_EXISTENT_KEY);
    }

    /**
     * @test
     */
    public function it_provides_offset_exists()
    {
        $this->assertTrue(isset($this->decorator[self::CUSTOM_KEY]));
        $this->assertFalse(isset($this->decorator[self::NON_EXISTENT_KEY]));

        $this->assertTrue($this->decorator->offsetExists(self::CUSTOM_KEY));
        $this->assertFalse($this->decorator->offsetExists(self::NON_EXISTENT_KEY));
    }

    /**
     * @test
     */
    public function it_provides_array_access_set()
    {
        $expectedValue = 'new-value';
        $key = 'new-key';
        $this->decorator[$key] = $expectedValue;
        $this->assertEquals($expectedValue, $this->decorator[$key]);
    }

    /**
     * @test
     */
    public function it_provides_offset_set()
    {
        $expectedValue = 'new-value';
        $key = 'new-key';
        $this->decorator->offsetSet($key, $expectedValue);
        $this->assertEquals($expectedValue, $this->decorator->offsetGet($key));
    }

    /**
     * @test
     */
    public function it_provides_array_access_unset()
    {
        unset($this->decorator[self::CUSTOM_KEY]);
        $this->assertFalse(isset($this->decorator[self::CUSTOM_KEY]));
    }

    /**
     * @test
     */
    public function it_provides_offset_unset()
    {
        $this->decorator->offsetUnset(self::CUSTOM_KEY);
        $this->assertFalse(isset($this->decorator[self::CUSTOM_KEY]));
    }
}
