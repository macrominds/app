<?php

namespace Tests\Unit\Services;

use Macrominds\Services\Container;
use Macrominds\Services\ServiceProvider;
use Tests\TestCase;

class ContainerBindingResolutionTest extends TestCase
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @before
     */
    public function setupContainer()
    {
        $this->container = $this->getContainer();
    }

    /**
     * @before
     */
    public function resetInstanceCounter()
    {
        FactoryObject::resetNumberOfInstances();
    }

    /**
     * @test
     */
    public function it_registers_service_providers_and_resolves_bindings()
    {
        $this->container->registerServiceProvider(new FactoryObjectServiceProvider());

        $this->assertTwoInstancesOfFactoryObjectThatAreDifferent();
    }

    private function assertTwoInstancesOfFactoryObjectThatAreDifferent()
    {
        $first = $this->container->resolve(FactoryObject::class);
        $second = $this->container->resolve(FactoryObject::class);

        $this->assertEquals(2, FactoryObject::getNumberOfInstances());
        $this->assertNotSame($first, $second);
    }

    /**
     * @test
     */
    public function it_binds_factory_objects_to_the_container()
    {
        $this->container->factoryObject(
            FactoryObject::class,
            function (): FactoryObject {
                return new FactoryObject();
            }
        );
        $this->assertTwoInstancesOfFactoryObjectThatAreDifferent();
    }

    /**
     * @test
     */
    public function it_binds_singletons_to_the_container()
    {
        $this->container->singleton(
            Singleton::class,
            function (): Singleton {
                return new Singleton();
            }
        );
        $firstSingleton = $this->container->resolve(Singleton::class);
        $secondSingleton = $this->container->resolve(Singleton::class);
        $this->assertEquals(1, Singleton::getNumberOfInstances());
        $this->assertSame($firstSingleton, $secondSingleton);
    }

    /**
     * @test
     */
    public function factory_object_method_is_fluid()
    {
        $container = $this->container->factoryObject(
            FactoryObject::class,
            function () {
                new FactoryObject();
            }
        );
        $this->assertSame($container, $this->container);
    }

    /**
     * @test
     */
    public function singleton_method_is_fluid()
    {
        $container = $this->container->singleton(
            Singleton::class,
            function () {
                new Singleton();
            }
        );
        $this->assertSame($container, $this->container);
    }
}

class FactoryObjectServiceProvider implements ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $container->factoryObject(
            FactoryObject::class,
            function (): FactoryObject {
                return new FactoryObject();
            }
        );
    }
}

class FactoryObject
{
    use CountInstances;
}

class Singleton
{
    use CountInstances;
}

trait CountInstances
{
    /**
     * @var int
     */
    private static $numInstances = 0;

    public function __construct()
    {
        ++self::$numInstances;
    }

    public static function getNumberOfInstances(): int
    {
        return self::$numInstances;
    }

    public static function resetNumberOfInstances()
    {
        self::$numInstances = 0;
    }
}
