<?php

namespace Tests\Unit\App;

use Macrominds\App\HasContainer;
use Macrominds\Services\Container;
use Tests\TestCase;

class HasContainerTest extends TestCase
{
    /**
     * @test
     */
    public function it_stores_a_container_instance_as_a_property()
    {
        $instance = new class($this->getContainer()) {
            use HasContainer;

            /**
             *  constructor.
             */
            public function __construct(Container $container)
            {
                $this->container = $container;
            }

            public function getContainer()
            {
                return $this->container;
            }
        };
        $this->assertNotNull($instance->getContainer());
    }

    /**
     * @test
     */
    public function it_creates_a_container()
    {
        $instance = new class() {
            use HasContainer;

            /**
             *  constructor.
             */
            public function __construct()
            {
                $this->container = $this->provideContainer();
            }

            public function getContainer()
            {
                return $this->container;
            }
        };
        $this->assertNotNull($instance->getContainer());
    }

    /**
     * @test
     */
    public function it_uses_the_given_container_or_creates_a_new_one()
    {
        $nullContainerClass = new DefaultHasContainerClass(null);
        $createdContainer = $nullContainerClass->getContainer();
        $this->assertNotNull($createdContainer);
        $this->assertInstanceOf(Container::class, $createdContainer);

        $providedContainerClass = new DefaultHasContainerClass($this->newContainer());
        $providedContainer = $providedContainerClass->getContainer();
        $this->assertNotNull($providedContainer);
        $this->assertNotSame(
            $createdContainer,
            $providedContainer,
            'Expected two different containers: the created singleton and the provided instance'
        );
    }
}

class DefaultHasContainerClass
{
    use HasContainer;

    /**
     *  constructor.
     */
    public function __construct(Container $container = null)
    {
        $this->container = $this->provideContainer($container);
    }

    public function getContainer()
    {
        return $this->container;
    }
}
