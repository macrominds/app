<?php

namespace Tests\Unit\App;

use Macrominds\App\ContainerProvider;
use Macrominds\Services\Container;
use Tests\TestCase;

class ContainerProviderTest extends TestCase
{
    /**
     * @test
     */
    public function it_provides_a_container()
    {
        $container = ContainerProvider::getInstance();
        $this->assertNotNull($container);
        $this->assertInstanceOf(Container::class, $container);
    }

    /**
     * @test
     */
    public function it_provides_a_singleton()
    {
        $firstInstance = ContainerProvider::getInstance();
        $secondInstance = ContainerProvider::getInstance();
        $this->assertSame(
            $firstInstance,
            $secondInstance,
            'Expected singleton but got two different instances'
        );
    }

    /**
     * @test
     */
    public function it_can_be_reset()
    {
        $firstInstance = ContainerProvider::getInstance();
        ContainerProvider::forgetInstance();
        $secondInstance = ContainerProvider::getInstance();
        $this->assertNotSame(
            $firstInstance,
            $secondInstance,
            'Expected different instances after reset but got identical instances'
        );
    }
}
