<?php


namespace Tests\Unit\App;

use Macrominds\App\HasContainer;
use Macrominds\App\HasDefaultProjectPath;
use Tests\TestCase;

class HasDefaultProjectPathTest extends TestCase
{
    /** @test */
    public function provides_a_project_path()
    {
        $this->assertSame(
            'project-path',
            (new HavingProjectPath('project-path'))
            ->exposeProjectPath()
        );
    }
}

class HavingProjectPath
{
    use HasContainer;
    use HasDefaultProjectPath;

    public function __construct(string $projectPath)
    {
        $this->container = $this->provideContainer();
        $this->registerProjectPathProvider($projectPath);
    }

    public function exposeProjectPath(string $subPathWithLeadingSlash = null): string
    {
        return $this->getProjectPath($subPathWithLeadingSlash);
    }
}
