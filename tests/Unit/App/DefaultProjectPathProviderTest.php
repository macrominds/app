<?php

namespace Tests\Unit\App;

use InvalidArgumentException;
use Macrominds\App\DefaultProjectPathProvider;
use Tests\TestCase;
use TypeError;

class DefaultProjectPathProviderTest extends TestCase
{
    /**
     * @var DefaultProjectPathProvider
     */
    private $projectPathProvider;

    /**
     * @before
     */
    public function setupProjectPathProvider()
    {
        $this->projectPathProvider = new DefaultProjectPathProvider(
            realpath(__DIR__.'/../../..')
        );
    }

    /**
     * @test
     * @dataProvider provideValidRelativePaths
     */
    public function it_converts_a_valid_relative_project_relative_path_to_an_absolute_path($validRelativePath)
    {
        $this->assertEquals(
            $this->getProjectRoot($validRelativePath),
            $this->projectPathProvider->getProjectPath($validRelativePath)
        );
    }

    public function provideValidRelativePaths()
    {
        return $this->indexedValueArrayToAssocDataProvider([
            '/',
            '/content',
            '/content/general',
            '/even-non-existing/paths-with.suffix',
        ]);
    }

    /**
     * @test
     */
    public function you_cannot_pass_null_when_querying_a_relative_project_path()
    {
        $this->expectException(TypeError::class);
        $this->projectPathProvider->relativeProjectPath(null);
    }

    /**
     * @test
     * @dataProvider provideInvalidPaths
     */
    public function it_handles_invalid_paths($invalidPath)
    {
        $this->expectException(InvalidArgumentException::class);
        $this->projectPathProvider->getProjectPath($invalidPath);
    }

    public function provideInvalidPaths()
    {
        return $this->indexedValueArrayToAssocDataProvider([
            '',
            'no-leading-slash',
            'no-leading/slash',
        ]);
    }

    /**
     * @test
     */
    public function it_returns_the_project_path_if_no_argument_is_passed()
    {
        $this->assertSame(
            $this->getProjectRoot(),
            $this->projectPathProvider->getProjectPath()
        );
    }

    /**
     * @test
     */
    public function it_returns_the_project_path_if_a_null_argument_is_passed()
    {
        $this->assertSame(
            $this->getProjectRoot(),
            $this->projectPathProvider->getProjectPath(null)
        );
    }
}
