<?php

namespace Tests\Unit\App;

use Macrominds\App\App;
use Macrominds\Services\AllowsServiceRegistration;
use Macrominds\Services\Container;
use Macrominds\Services\ServiceProvider;
use Tests\TestCase;

class AppCustomServiceProviderTest extends TestCase
{
    /**
     * @var SimpleApp
     */
    private $app;
    /**
     * @var array
     */
    private $serviceProviderArray;

    /**
     * @before
     */
    public function setupApp()
    {
        $this->app = new SimpleApp('dummypath');
    }

    /**
     * @before
     */
    public function setupCustomServiceProviderArray()
    {
        $this->serviceProviderArray = [
            'custom' => new CustomServiceProvider(),
            'another' => new AnotherServiceProvider(),
        ];
    }

    /**
     * @test
     */
    public function it_registers_a_service_provider()
    {
        foreach ($this->serviceProviderArray as $key => $serviceProvider) {
            $this->app->registerServiceProvider($serviceProvider);
            $this->assertServiceHasBeenProvided($key);
        }
    }

    /**
     * @test
     */
    public function it_registers_multiple_service_providers_at_once()
    {
        $this->app->registerServiceProviders(array_values($this->serviceProviderArray));
        $this->assertServicesHaveBeenProvided(array_keys($this->serviceProviderArray));
    }

    private function assertServiceHasBeenProvided(string $key)
    {
        $this->assertEquals("${key}-value", $this->app->getContainer()["${key}-key"]);
    }

    private function assertServicesHaveBeenProvided(array $keys)
    {
        foreach ($keys as $key) {
            $this->assertEquals("${key}-value", $this->app->getContainer()["${key}-key"]);
        }
    }
}

class SimpleApp extends App
{
    use AllowsServiceRegistration;

    protected function registerServices(string $projectPath)
    {
    }
}

class CustomServiceProvider implements ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $container['custom-key'] = 'custom-value';
    }
}

class AnotherServiceProvider implements ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $container['another-key'] = 'another-value';
    }
}
