<?php

namespace Tests;

use Macrominds\App\DefaultProjectPathProvider;
use Macrominds\App\ProjectPathProvider;
use Macrominds\Config\Env;
use Macrominds\App\ContainerProvider;
use Macrominds\Testing\ProvidesContainer;

class TestCase extends \PHPUnit\Framework\TestCase
{
    use ProvidesContainer;

    protected function initializeContainerProvider(): void
    {
        $container = ContainerProvider::getInstance();
        $container->singleton(
            Env::class,
            function (): Env {
                return new Env($this->getProjectRoot(), $this->getDotEnvTestingPath());
            }
        );
        $container->singleton(
            ProjectPathProvider::class,
            function (): ProjectPathProvider {
                return new DefaultProjectPathProvider($this->getProjectRoot());
            }
        );
    }

    protected function getFixturesPath(string $relPathLeadingSlash = ''): string
    {
        return $this->getProjectRoot("/tests/fixtures{$relPathLeadingSlash}");
    }

    protected function getProjectRoot(string $relPathLeadingSlash = ''): string
    {
        return realpath(__DIR__.'/..').$relPathLeadingSlash;
    }

    protected function getDotEnvTestingPath(): string
    {
        return '/tests/fixtures/config/dotenv/.env.testing';
    }

    protected function getConfigPath(string $relPathLeadingSlash): string
    {
        return $this->getFixturesPath("/config{$relPathLeadingSlash}");
    }

    protected function indexedValueArrayToAssocDataProvider(array $indexedValueArray): array
    {
        return array_combine(
            $indexedValueArray,
            array_map(
                function ($item) {
                    return [$item];
                },
                $indexedValueArray
            )
        );
    }
}
