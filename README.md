# Macrominds App

Basic lib to simplify App setup with IoC, .env and configuration

## Usage 

Create your own app and add the traits you need. The default basic `App` already
provides `use HasContainer`.

```php
use Macrominds\App\App;

class MyApp extends App {
    use \Macrominds\Config\UsesDotEnv;
    use \Macrominds\Config\IsConfigurable;
    use \Macrominds\Services\AllowsServiceRegistration;
    use \Macrominds\App\HasDefaultProjectPath;

    protected function registerServices(string $projectPath) 
    {
        $this->registerDotEnv($projectPath);
        $this->registerDefaultConfig();
        $this->registerProjectPathProvider($projectPath);
    }
}
```

## Coding Style

Run `vendor/bin/php-cs-fixer fix .` to apply coding style fixes.

## Testing this Project

Run `./vendor/bin/phpunit` to run the tests.

## Automatic testing and docker images

This project provides a [.gitlab-ci.yml](.gitlab-ci.yml) and thus uses GitLab CI/CD.

To build the images locally:

```bash
$ ./ci.bash build-all-local
```

Run phpunit inside the containers:

```bash
$ ./ci.bash test-all-local
```

Build and publish the images for gitlab CI 
([also see gitlab docs](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)):

```bash
$ ./ci.bash build-all-remotes-and-publish
```

### Reuse testing setup for your projects

Include 

- [ci/.composer-template.yml](ci/.composer-template.yml)
- [ci/.testing-template.yml](ci/.testing-template.yml)

to your project's .gitlab-ci.yml.

If necessary, override the used variables.

See [.gitlab-ci.yml](.gitlab-ci.yml) for an example of local includes and see the [GitLab documentation for includes](https://docs.gitlab.com/ee/ci/yaml/includes.html) for a deep dive into local and remote include options and how to override variables.

## Metrics and Coverage

- metrics: `composer app:phpmetrics`
- coverage report: `composer app:coverage`
The reports are stored in the `report` directory. 
Both scripts will output the path to the reports after successful completion.
