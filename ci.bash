#!/usr/bin/env bash

# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

# overridable basics
DOCKER_CMD=${DOCKER_CMD:-docker}
GITLAB_REGISTRY_PREFIX=${GITLAB_REGISTRY_PREFIX:-"registry.gitlab.com"}
IMAGE_BASE_NAME=${IMAGE_BASE_NAME:-"macrominds/app"}

# associative array for php version aliases
declare -A PHP_BUILD_PARAMETERS
PHP_BUILD_PARAMETERS=(
  [php80]="8.0 2"
  [php81]="8.1 2"
  [php82]="8.2 2"
)

# array of php version aliases, read from the keys of PHP_BUILD_PARAMETERS
IFS=" " read -r -a PHP_VERSION_ALIASES <<<"${!PHP_BUILD_PARAMETERS[@]}"

function build-all-local() {
  build-all "${IMAGE_BASE_NAME}"
}

function build-all() {
  local image_base_name="${1}"
  for php_version_alias in "${PHP_VERSION_ALIASES[@]}"; do
    local build_arguments
    IFS=" " read -r -a build_arguments <<<"${PHP_BUILD_PARAMETERS[$php_version_alias]}"
    build "${DOCKER_CMD}" "${image_base_name}:${php_version_alias}" "${build_arguments[@]}"
  done
}

function build() {
  local docker_cmd=${1}
  local image="${2}"
  local php_version_alias="${3}"
  local composer_version="${4}"
  ${docker_cmd} build -t "${image}" \
    --file ci/docker/php.df \
    --build-arg PHP_VERSION="${php_version_alias}" \
    --build-arg COMPOSER_VERSION="${composer_version}" \
    ci/docker
}

function build-all-remotes-and-publish() {
  build-all "${GITLAB_REGISTRY_PREFIX}/${IMAGE_BASE_NAME}"
  publish-all "${GITLAB_REGISTRY_PREFIX}/${IMAGE_BASE_NAME}"
}

function publish-all() {
  local image_base_name="${1}"
  ${DOCKER_CMD} login "${GITLAB_REGISTRY_PREFIX}"
  for php_version_alias in "${PHP_VERSION_ALIASES[@]}"; do
    ${DOCKER_CMD} push "${image_base_name}:${php_version_alias}"
  done
}

function test-all-local() {
  for php_version_alias in "${PHP_VERSION_ALIASES[@]}"; do
    test-local "${php_version_alias}"
  done
}

function test-local() {
  local php_version_alias="${1}"
  "${DOCKER_CMD}" run -it --rm -v "$(pwd)":/var/www/html "${IMAGE_BASE_NAME}:${php_version_alias}" vendor/bin/phpunit
}

# TODO print usage
$1 "${@:2}"
